package org.lightning.paywall.paywalltademo.controller;

import org.lightning.paywall.paywalltademo.entity.DataForwarder;
import org.lightningj.paywall.annotations.PaymentRequired;
import org.lightningj.paywall.vo.OrderRequest;
import org.lightningj.paywall.vo.PaymentOption;
import org.lightningj.paywall.web.CachableHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;


public class CustomOrderRequest implements org.lightningj.paywall.orderrequestgenerator.OrderRequestGenerator {

    DataForwarder.ProductForward  productForward = new DataForwarder.ProductForward();

    @Override
    public OrderRequest generate(PaymentRequired paymentRequired, CachableHttpServletRequest request) {
        String article = productForward.getArticle();
        int units = productForward.getUnits().intValue();
        productForward.unset();

        return new OrderRequest(
                article,
                units,
                new ArrayList<PaymentOption>(),
                true
        );
    }
}