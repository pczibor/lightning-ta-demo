package org.lightning.paywall.paywalltademo.controller;

import org.lightning.paywall.paywalltademo.model.TADemoResult;
import org.lightningj.paywall.annotations.PaymentRequired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class TADemoRestController {
    private static final String template = "Pris is going: %s";
    private final AtomicLong counter = new AtomicLong();
    private final SecureRandom taEngine = new SecureRandom();

    @PaymentRequired(articleId = "snickers", payPerRequest = true)
    @RequestMapping("/tademo")
    public TADemoResult tademo() {
        boolean goingUp = taEngine.nextBoolean();
        return new TADemoResult(counter.incrementAndGet(),
                String.format(template, (goingUp ? "up": "down")),
                goingUp);
    }
}
