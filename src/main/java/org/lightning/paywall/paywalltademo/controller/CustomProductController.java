package org.lightning.paywall.paywalltademo.controller;

import org.lightning.paywall.paywalltademo.entity.DataForwarder;
import org.lightning.paywall.paywalltademo.repository.ArticleDataRepository;
import org.lightningj.paywall.annotations.PaymentRequired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class CustomProductController {
    DataForwarder.ProductForward  productForward = new DataForwarder.ProductForward();

    @Autowired
    ArticleDataRepository articleDataRepository;

    @RequestMapping("/buy")
    public String buyProduct() {
        //Iterable<ArticleData> articles = articleDataRepository.findAll();

        return "buy";
    }

    @PostMapping("buy")
    public RedirectView processOrder() {
        productForward.setArticle("snickers");
        productForward.setUnits(2);

        return new RedirectView("/buy/checkout");
    }

    @PaymentRequired(orderRequestGenerator = CustomOrderRequest.class)
    @RequestMapping("/buy/checkout")
    public String checkout() {
        return "Thank you";
    }
}
