package org.lightning.paywall.paywalltademo.repository;

import org.lightning.paywall.paywalltademo.entity.DemoPerRequestPaymentData;
import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository for DemoPerRequestPaymentData.
 */
public interface DemoPerRequestPaymentDataRepository extends CrudRepository<DemoPerRequestPaymentData,Integer> {

    DemoPerRequestPaymentData findByPreImageHash(String preImageHash);
}
