package org.lightning.paywall.paywalltademo.repository;

import org.lightning.paywall.paywalltademo.entity.ArticleData;
import org.springframework.data.repository.CrudRepository;

public interface ArticleDataRepository extends CrudRepository<ArticleData,Integer> {
    ArticleData findByArticleId(String articleId);
    Iterable<ArticleData> findAll();
}