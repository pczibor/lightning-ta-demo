package org.lightning.paywall.paywalltademo.entity;

import org.springframework.stereotype.Component;

@Component("productForward")
public class DataForwarder {
    public static class ProductForward {
        private static String article;
        private static Integer units;

        public String getArticle() {
            System.out.println("Getting article!");
            return article;
        }

        public void setArticle(String article) {
            System.out.println("Setting article!");
            this.article = article;
        }

        public Integer getUnits() {
            return units;
        }

        public void setUnits(Integer units) {
            this.units = units;
        }

        public void unset() {
            article = null;
            units = null;
        }
    }
}