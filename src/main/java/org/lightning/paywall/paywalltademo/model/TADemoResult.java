package org.lightning.paywall.paywalltademo.model;

public class TADemoResult {
    private long id;
    private String prediction;
    private boolean goingUp;

    public TADemoResult(long id, String prediction, boolean goingUp) {
        this.id = id;
        this.prediction = prediction;
        this.goingUp = goingUp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrediction() {
        return prediction;
    }

    public void setPrediction(String prediction) {
        this.prediction = prediction;
    }

    public boolean isGoingUp() {
        return goingUp;
    }

    public void setGoingUp(boolean goingUp) {
        this.goingUp = goingUp;
    }
}
